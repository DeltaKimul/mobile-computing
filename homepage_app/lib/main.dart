import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HomePage',
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Color(0xff2A2E43),
      appBar: new AppBar(
        title: Text("Teropong Dunia"),
        elevation: 0,
        backgroundColor: Color(0xff2A2E43),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 24.0),
            child: Icon(Icons.search),
          ),
        ],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: Text("M. FIkri Mulyawan"),
              accountEmail: Text("m.fikri1006@student.unila.ac.id"),
              currentAccountPicture: new CircleAvatar(
                backgroundColor: Colors.green,
              ),
              decoration: BoxDecoration(
                color: Color(0xff2A2E43)
              ),
            ),

            new ListTile(
              leading: new Icon(Icons.home),
              title: new Text("Home"),
              onTap: (){
                Navigator.pop(context);
              },
            ),

            new ListTile(
              leading: new Icon(Icons.info_outline),
              title: new Text("About Us"),
              onTap: (){
                Navigator.pop(context);
              },
            ),

            new ListTile(
              leading: new Icon(Icons.announcement),
              title: new Text("Privacy Policy"),
              onTap: (){
                Navigator.pop(context);
              },
            ),
            new Divider(
              color: Color(0xff2A2E43),
            ),
            new ListTile(
              leading: new Icon(Icons.exit_to_app),
              title: new Text("Log Out"),
              onTap: (){
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      
    );
  }
}