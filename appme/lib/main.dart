import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Profile Dashboard',
      debugShowCheckedModeBanner: false,
      home: MyProfile(),
    );
  }
}

class MyProfile extends StatelessWidget {
  List gambar = [
    'Animasi/Cartoon/Anime',
    'Gaming',
    'Karikatur',
    'Bangunan Klasik',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff2A2E43),
      appBar: AppBar(
        leading: Icon(Icons.arrow_back,),
        elevation: 0,
        backgroundColor: Color(0xff2A2E43),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.filter_list,),
          )
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          //Profile name
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left:24.0 , top:5),
                child: CircleAvatar(
                  radius: 30,
                  backgroundImage: AssetImage('assets/images/icon.PNG'),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left:16.0),
                    child: Text(
                      'M. Fikri Mulyawan',
                      style:
                      TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 24,
                          color: Colors.white
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left:15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.location_on, color: Colors.white, size: 14,),
                        Padding(
                          padding: const EdgeInsets.only(left:4.0),
                          child: Text('Bandar Lampung- Lampung',
                          style:
                            TextStyle(
                              fontSize: 14,
                              wordSpacing: 2,
                              letterSpacing: 4,
                              color: Colors.white
                            ),),
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
            ),

          //main section
          Expanded(
            child: Container(
              width: double.infinity,
              margin: EdgeInsets.only(top: 33),
              decoration: BoxDecoration(
                color: Color(0xffF5F6F9),
                borderRadius: BorderRadius.vertical(top: Radius.circular(13))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left:24.0 , top:22.0),
                    child: Text('Photo & Video',
                    style:
                      TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  Container(
                    height: 40,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: gambar.length,
                        itemBuilder: (BuildContext context, int index){
                          return
                            index==1?
                            Padding(
                              padding: const EdgeInsets.only(right:20.0 ,top:10.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(gambar[index],style: TextStyle(color: Color(0xff434AE8), fontSize: 19)),
                                  CircleAvatar(
                                    radius: 2,
                                    backgroundColor: Color(0xff434AE8),
                                  )
                                ],
                              ),
                            )
                            :Padding(
                              padding: const EdgeInsets.only(right:20.0 ,top:10.0),
                              child: Text(gambar[index],
                                style: TextStyle(
                                    color: Colors.grey.withOpacity(0.9),
                                    fontSize: 19)),
                            );
                        },
                    )
                  ),
                  Expanded(
                    child: Stack(alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      Align(alignment: Alignment.topCenter,
                      child: Container(
                        padding: EdgeInsets.only(right: 25, left:25),
                        height: 450,
                        child: StaggeredGridView.countBuilder(
                            crossAxisCount: 4,
                            itemCount: 4,
                            itemBuilder: (BuildContext context, int index) => Container(
                              child: ClipRRect(
                                borderRadius: BorderRadius.all(Radius.circular(12)),
                                child: Image.asset('assets/collection/collection${index + 1}.png', fit: BoxFit.cover,),
                              ),
                            ),
                          staggeredTileBuilder: (int index) =>
                              StaggeredTile.count(
                                  2, index.isEven ? 3 : 1),
                          mainAxisSpacing: 9,
                          crossAxisSpacing: 8,
                        ),
                      ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.only(right: 12, left:12),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(33))),
                            height: 55,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Icon(Icons.home, color: Color(0xff434AE8)),
                                Icon(Icons.star_border, color: Colors.grey.withOpacity(0.6)),
                                SizedBox(
                                  width: 30,
                                ),
                                Icon(Icons.people_outline, color: Colors.grey.withOpacity(0.6)),
                                Icon(Icons.folder_open, color: Colors.grey.withOpacity(0.6))
                              ],
                            ),
                      ),
                      Positioned(
                        bottom: 23,
                        child: Container(
                          height: 66,
                          width: 66,
                          child: Icon(Icons.add, size: 27, color: Colors.white),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                            gradient: LinearGradient(
                              colors: [
                                Color(0xff6615C1),
                                Color(0xff484FDE)
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            )
                          ),
                        ),
                      )
                    ],
                    ),
                  )
                ],
              ),
            ),
          )
          ],
      ));
  }
}
