
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Profile Dashboard',
      debugShowCheckedModeBanner: false,
      home: MyLogin(),
    );
  }
}

class MyLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff2A2E43),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left:38, top: 44.0, right:49.0, bottom:41.0 ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 14.0, top: 7.0, right: 12.0, bottom: 9.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      color: Color(0xff5773FF)
                  ),
                  child:
                  Text("SIGN UP",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1,
                        fontSize: 22,
                        color: Colors.white
                    ),
                  ),
                ),

                Container(
                  color: Colors.white,
                  width: 0.2,
                  height: 22,
                ),

                Container(

                  child:
                  Text("SIGN IN",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1,
                        fontSize: 22,
                        color: Colors.white
                    ),
                  ),

                )
              ],
            ),
          ),

          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Form(
                  child: new Theme(
                    data: new ThemeData(
                        brightness: Brightness.dark,
                        primarySwatch: Colors.teal,
                        inputDecorationTheme: new InputDecorationTheme(
                            labelStyle: new TextStyle(
                                color: Colors.teal,
                                fontSize: 24))),
                    child: Padding(
                      padding: const EdgeInsets.only(right: 48.0, bottom: 41.0, left: 48.0, top: 41.0),
                      child: new Column(
                        children: <Widget>[
                          new TextFormField(
                            decoration: new InputDecoration(
                              labelText: "Nama",
                            ),
                            keyboardType: TextInputType.text,
                          ),

                          new TextFormField(
                            decoration: new InputDecoration(
                              labelText: "Email",
                            ),
                            keyboardType: TextInputType.emailAddress,
                          ),

                          new TextFormField(
                            decoration: new InputDecoration(
                              labelText: "Password",
                            ),
                            keyboardType: TextInputType.text,
                            obscureText: true,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 41.0),
                            child: new MaterialButton(
                              height: 52,
                              color: Color(0xff3ACCE1),
                              textColor: Colors.white,
                              child: Text("CONTINUE"),
                              onPressed: () => {},
                              splashColor: Colors.redAccent,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
              )
            ],
          )


        ],
      ),
    );
  }
}